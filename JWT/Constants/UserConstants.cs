﻿using JWT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JWT.Constants
{
    public static class UserConstants
    {
        public static List<UserModel> Users = new List<UserModel>()
        {
            new UserModel()
            {
                Username = "admin",
                Password = "admin",
                Email = "admin@gmail.com",
                GivenName = "Administrator",
                Surname = "Admin",
                Role = "Administrator"
            },
             new UserModel()
            {
                Username = "system",
                Password = "system",
                Email = "system@gmail.com",
                GivenName = "SystemGiveName",
                Surname = "SystemSurname",
                Role = "System"
            },
              new UserModel()
            {
                Username = "test",
                Password = "test",
                Email = "test@gmail.com",
                GivenName = "testGiveName",
                Surname = "testSureName",
                Role = "test"
            }
        };
    }
}
