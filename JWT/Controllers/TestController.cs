﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JWT.Controllers
{
    //[Route("api/[controller]")]
    //[ApiController]
    public class TestController : BaseController
    {
        [HttpGet("test")]
        public async Task<IActionResult> Test()
        {
            return Ok("Ok, you're on public property");
        }
    }
}
