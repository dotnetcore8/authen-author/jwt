﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JWT.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        public new IActionResult Response<T>(List<T> t)
        {
            return Ok(t);
        }

        public new IActionResult Response<T>(T t)
        {
            return Ok(t);
        }
    }
}
