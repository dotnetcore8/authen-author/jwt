﻿using JWT.Constants;
using JWT.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace JWT.Controllers
{
    //[Route("api/[controller]")]
    //[ApiController]
    public class LoginController : BaseController
    {
        private IConfiguration _config;
        
        public LoginController(IConfiguration config)
        {
            this._config = config;
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult Login([FromBody] UserLogin userLogin)
        {
            var user = Authentication(userLogin);
            if(user != null)
            {
                var token = GenerateToken(user);
                return Ok(token);
            }
            return NotFound("User not found");
        }    

        private string GenerateToken(UserModel user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.Username),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.GivenName, user.GivenName),
                new Claim(ClaimTypes.Surname, user.Surname),
                new Claim(ClaimTypes.Role, user.Role),
            };
            var expiresToken = 15; //Munimus
            var token = new JwtSecurityToken(_config["Jwt:Issuer"], 
                _config["Jwt:Audience"],
                claims, 
                expires: DateTime.Now.AddMinutes(expiresToken), 
                signingCredentials: credentials);
            var genTokenResult = new JwtSecurityTokenHandler().WriteToken(token);
            return genTokenResult;
        }

        private UserModel Authentication(UserLogin userlogin)
        {
            var currentUser = UserConstants.Users.FirstOrDefault(e => e.Username.Equals(userlogin.userName) && e.Password.Equals(userlogin.passWord));
            if(currentUser != null)
            {
                return currentUser;
            }
            return null;
        }
    }
}
