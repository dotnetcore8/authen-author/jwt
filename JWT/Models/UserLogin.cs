﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JWT.Models
{
    public class UserLogin
    {
        public string userName { get; set; }
        public string passWord { get; set; }
    }
}
