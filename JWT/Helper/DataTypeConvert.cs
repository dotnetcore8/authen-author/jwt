﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JWT.Helper
{
    public static class DataTypeConvert
    {
        public static int ToInt32(this string input)
        {
            throw new Exception();
            return Convert.ToInt32(input);
        }
        public static int ToInt32(this bool input)
        {
            return Convert.ToInt32(input);
        }
        public static int ToInt32(this double input)
        {
            return Convert.ToInt32(input);
        }
        public static int ToInt32(this char input)
        {
            return Convert.ToInt32(input);
        }
    }
}
